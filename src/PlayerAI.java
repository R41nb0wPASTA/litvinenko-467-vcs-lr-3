import java.awt.*;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Map;

public class PlayerAI extends Player {
    /**
     * Previously hitted cell by AI Player
     */
    private ArrayList<Cell> previouslyHittedCells = new ArrayList<>();

    @Override
    public boolean makeMove(Cell cell) {
        boolean isAHit = false;

        //Last cell anti random hitter
        if(getHittedAliveCells() == game.getFieldSize() * game.getFieldSize() - 1) {
            Map<Point, Cell> temp = game.getAliveGameField().getFieldCells();

            for (int i = 0; i < game.getFieldSize(); i++) {
                for (int j = 0; j < game.getFieldSize(); j++) {
                    if (!temp.get(new Point(i, j)).isHitted()) {
                        isAHit = checkForHit(temp.get(new Point(i, j)));
                    }
                }
            }

            isAHit = false;
        }
        //If previous hit wasn't the first one
        else if(getLastHittedShipCell() != null && !ShipPart.isShipWrecked(getLastHittedShipCell())) {
            Map<Direction, Cell> neighbours = getLastHittedShipCell().getNeighborCells();

            Boolean isFirst = true;
            for (Map.Entry<Direction, Cell> entry : neighbours.entrySet()) {
                //If already hitted nearby
                if (entry.getValue().getUnitType() == UnitType.BROKEN_SHIP_PART) {
                    isFirst = false;
                    Direction dir = entry.getKey().getOppositeDirection();
                    //Try to hit in another dir
                    try {
                        Cell temp = getLastHittedShipCell().getNeighborCell(dir);
                        if (temp.isHitted()) {
                            dir = dir.getOppositeDirection();
                            do {
                                temp = temp.getNeighborCell(dir);
                            } while (temp.getNeighborCell(dir).getUnitType() == UnitType.BROKEN_SHIP_PART);
                            temp = temp.getNeighborCell(dir);

                            isAHit = checkForHit(temp);

                        }
                        else
                            isAHit = checkForHit(temp);
                    }
                    //If got out from the field - go to the last hitted end from the opposite dir
                    catch (Exception e) {
                        Cell temp = getLastHittedShipCell();
                        dir = dir.getOppositeDirection();

                        do {
                            temp = temp.getNeighborCell(dir);
                        } while (temp.getNeighborCell(dir).getUnitType() == UnitType.BROKEN_SHIP_PART);

                        temp = temp.getNeighborCell(dir);

                        isAHit = checkForHit(temp);
                    }
                    break;
                }

            }

            //If previous hit was the first one
            if(isFirst) {
                for (Map.Entry<Direction, Cell> entry : neighbours.entrySet()) {
                    if(!entry.getValue().isHitted()) {
                        isAHit = checkForHit(entry.getValue());
                        break;
                    }
                }
            }
        }
        //Random hit
        else {
            SecureRandom random = new SecureRandom();
            int x;
            int y;

            do{
                x = random.nextInt(game.getFieldSize());
                y = random.nextInt(game.getFieldSize());
            }
            while(game.getAliveGameField().getFieldCells().get(new Point(x, y)).isHitted());

            Cell temp = game.getAliveGameField().getFieldCells().get(new Point(x, y));
            isAHit = checkForHit(temp);
        }

        return isAHit;
    }

    public void addHittedCell(Cell cell) { previouslyHittedCells.add(cell); }

    public ArrayList<Cell> getPreviouslyHittedCells() { return previouslyHittedCells; }

    private Cell getLastHittedShipCell() {
        Cell returnStatement = null;

        for (int i = previouslyHittedCells.size() - 1; i >= 0; i--) {
            if (previouslyHittedCells.get(i).getUnitType() == UnitType.BROKEN_SHIP_PART) {
                returnStatement = previouslyHittedCells.get(i);
                break;
            }
        }

        return returnStatement;
    }

    private boolean checkForHit(Cell cell) {
        boolean returnStatement = false;

        if(cell.getUnitType() == UnitType.SHIP_PART){
            cell.hit();
            cell.getUnit().destroy();
            game.getAliveGameField().decreaseUnbrokenShipParts();
            cell.setUnit(UnitType.BROKEN_SHIP_PART);

            returnStatement = true;
        }
        else if(cell.getUnitType() == UnitType.NAVAL_MINE) {
            cell.getUnit().destroy();
            int hittedShipParts = NavalMine.explode(cell, this);

            if(hittedShipParts > 0) {
                returnStatement = true;
                for(int i=0; i<hittedShipParts; i++) {
                    game.getAliveGameField().decreaseUnbrokenShipParts();
                }
            }
        }
        else if (cell.getUnitType() == UnitType.ISLAND) {
            cell.hit();
            cell.getUnit().destroy();
        }
        else if (cell.getUnitType() == UnitType.LIGHTHOUSE) {
            cell.hit();
            cell.getUnit().destroy();
        }
        else {
            cell.hit();
        }

        previouslyHittedCells.add(cell);

        return returnStatement;
    }

    private int getHittedAliveCells() {
        int returnStatement = 0;
        Map<Point, Cell> temp = game.getAliveGameField().getFieldCells();

        for (Map.Entry<Point, Cell> entry : temp.entrySet()) {
            if(entry.getValue().isHitted()) {
                returnStatement++;
            }
        }

        return returnStatement;
    }
}
