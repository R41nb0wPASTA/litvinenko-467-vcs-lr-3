import javax.swing.*;
import java.awt.*;

public class CustomJLabel {
    public static JLabel createCustomLabel(String text, Font font){
        if(text == null || text.isBlank())
            throw new IncorrectInputException("Text can't be blank!");
        if(font == null)
            throw new IllegalArgumentException("Font can't be null!");

        JLabel label = new JLabel(text);
        label.setFont(font);

        return label;
    }
}
