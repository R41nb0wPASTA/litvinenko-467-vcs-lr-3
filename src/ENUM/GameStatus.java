public enum GameStatus {
    WINNER_FOUND,
    GAME_IS_ON,
    GAME_ABORTED
}
